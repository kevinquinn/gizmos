#!/bin/bash
#
# (kq) 02042013
#
#


[ "$#" -eq 1 ] || die "You must specify the source and destination path with as following: 
		       $0 ${ROOT_DIR}/source ${ROOT_DIR}/destination.  Be sure to exclude a trailing slash as we will
		       handle that for you."

while getopts ":s:d:" params
  do
    case $params in
      "s")
	SRC_DIR="${OPTARG}"
        ;;
      "d")
	DEST_DIR="${OPTARG}"
        ;;
      "?")
        echo "Unknown option $OPTARG"
	exit 0
	;;
      ":")
        echo "Argument missing for value $OPTARG"
        ;;
      "*")
        echo "exception caught"
        ;;
    esac
done

for i in `find ${SRC_DIR} -maxdepth 1 -type f -mtime +3` ; do

        if [ `stat -c %s ${i}` -gt 1024 ] ; then

                FILE_NAME_ONLY="${i##*/}"

 		echo "The file name post transformation is ${FILE_NAME_ONLY}"

                gzip -c > "${DEST_DIR}/${FILE_NAME_ONLY}.gz" < "${i}"

        else
                cp -rp "${i}" "${DEST_DIR}"

        fi
done
