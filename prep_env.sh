#!/bin/bash

# set -x

# setup our testing directories in tmp
ROOT_DIR="/tmp/prj/`date +%F`"
NUBER_RANGE="0 1 2 3 4 5 6 7 8 9"
mkdir -p "${ROOT_DIR}/source"
mkdir -p "${ROOT_DIR}/destination"

# to test for the future:
# mkdir -p $ROOT_DIR
# if [ $? -ne 0 ] ; then 
# 	echo "FAIL"
# fi

# this creates the test env for the file magic script
cd "${ROOT_DIR}/source/"

echo "moved to source directory"

for i in 0 1 2 3 4 5 6 7 8 9 ; do dd if=/dev/zero of=large_${i} bs=1k count=1${i}6${i} ; done
echo "created files larger than 1Mb (not MiB)"

for i in 0 1 2 3 4 5 6 7 8 9 ; do dd if=/dev/zero of=mixed_${i} bs=1k count=10${i} ; done
echo "created smaller fileset"

for i in 0 1 2 3 4 5 6 7 8 9 ; do dd if=/dev/zero of=small_old_${i} bs=1k count=10${i} ; touch -m -t 01012013 small_old_${i} ; done
echo "created smaller and older fileset"

for i in 0 1 2 3 4 5 6 7 8 9 ; do dd if=/dev/zero of=large_old_${i} bs=1k count=8${i}9${i} ; touch -m -t 01012013 large_old_${i} ; done

# sanity check the sizes
ls -lh ; du -sh ${ROOT_DIR}
